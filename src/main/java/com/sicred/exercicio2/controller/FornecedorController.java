package com.sicred.exercicio2.controller;

import com.sicred.exercicio2.model.Fornecedor;
import com.sicred.exercicio2.model.Produto;
import com.sicred.exercicio2.service.FornecedorService;
import com.sicred.exercicio2.service.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/fornecedor")
public class FornecedorController {
    @Autowired
    private FornecedorService fornecedorService;

    @GetMapping
    public List<Fornecedor> listAllProdutos() {
        return fornecedorService.listAllFornecedors();
    }

    @PostMapping
    public ResponseEntity<Fornecedor> addProduto(@RequestBody Fornecedor fornecedor){
        Fornecedor fornecedorAdd = fornecedorService.addFornecedor(fornecedor);
        return ResponseEntity.status(HttpStatus.CREATED).body(fornecedorAdd);
    }
}
