package com.sicred.exercicio2.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@Entity
@Table(name = "PRODUTOS")
public class Produto {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nome;
    private Tipo tipo;
    private String familia;
    private Date ano_compra;
    private Double preco;
    private Date data_validade;
    private Boolean status;

}
